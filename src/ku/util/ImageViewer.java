package ku.util;

import java.awt.GridLayout;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ImageViewer extends JFrame implements Runnable{

	private JLabel show;
	private JPanel pane;

	public ImageViewer() {
		super("Image Preview");
		initComponents();
	}

	private void initComponents() {
		this.setLayout(new GridLayout(1,1));
		pane = new JPanel();
		this.add(pane);
		show = new JLabel("Drop picture here!!");
		pane.add(show);
		this.add(new JScrollPane(pane));
		createDropFileArea();
	}

	@Override
	public void run() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		this.setBounds(100, 100, 300, 400);
	}

	public static void main(String[] args) {
		ImageViewer fdui = new ImageViewer();
		fdui.run();
	}
	
	public void createDropFileArea(){
		new FileDrop(pane,
				new FileDrop.Listener() {
					private ImageFactory factory;

					public void filesDropped(java.io.File[] files) {
						factory = new CachingImageFactory();
						for (File file : files) {
							show.setText("");
							try {
								show.setIcon(factory.getIcon(new URL("file:\\\\\\"+file.getPath())));
							} catch (MalformedURLException e) {
								JOptionPane.showMessageDialog(pane, "Malformed picture!!",
										"Error", JOptionPane.ERROR_MESSAGE);
							}
						} // end for: through each dropped file
					} // end filesDropped
				}); // end FileDrop.Listener
	}

}
