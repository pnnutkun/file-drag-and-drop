package ku.util;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ImageFactory {
	private static ImageFactory factory;
	private Map<URL,Icon> icons;

	protected ImageFactory(){
		icons = new HashMap<URL,Icon>();
	}

	public static ImageFactory getInstance(){
		if(factory == null) return new ImageFactory();
		return null;
	}

	public Icon getIcon(URL url){
		ImageIcon icon = new ImageIcon( url.getPath() );
		putToMap(url, icon);
		return icon;
	}
	
	public Icon getSameIcon(URL url){
		return icons.get(url);
	}
	
	public void putToMap(URL url, Icon icon){
		icons.put(url, icon);
	}
	
	public boolean mapContainsKey(URL url){
		return icons.containsKey(url);
	}
}
