package ku.util;

import java.net.URL;
import javax.swing.Icon;

public class CachingImageFactory extends ImageFactory {

	public CachingImageFactory() {
		super.getInstance();
	}

	@Override
	public Icon getIcon(URL url) {
		if( super.mapContainsKey(url) ){
			return super.getSameIcon(url);
		}
		return super.getIcon(url);
	}
	
	
}
